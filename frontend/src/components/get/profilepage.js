import {AiFillHome}  from "react-icons/ai";
import {
  Table,
  TableHead,
  TableCell,
  TableRow,
  TableBody,
  Button,
  makeStyles,
} from "@material-ui/core";
import { Link } from "react-router-dom";


const useStyles = makeStyles({
  table: {
    width: "100%",
    margin: "auto",
    backgroundColor:"white",
  },
  thead: {
    "& > *": {
      fontSize: 15,
      background: "pink",
      color: "#FFFFFF",
    },
  },
  row: {
    "& > *": {
      fontSize: 13,
    },
  },
});

const Employees_details = () => {
  const classes = useStyles();
  const data = localStorage.getItem("empdata")
  const empdata = JSON.parse(data)
  console.log(empdata)
 return (
   <div>
    <Table className={classes.table}>
        <TableHead>
        <TableRow className={classes.thead}>
          <TableCell>FirstName</TableCell>
          <TableCell>LastName</TableCell>
          <TableCell>Email</TableCell>
          <TableCell>Phone number</TableCell>
          <TableCell>Department</TableCell>
          <TableCell>Project</TableCell>
          <TableCell>Edit Emp</TableCell>
          <TableCell>Delete Emp</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        <TableRow className={classes.row}>
          <TableCell>{empdata.obj.firstname}</TableCell>
          <TableCell>{empdata.obj.lastname}</TableCell>
          <TableCell>{empdata.obj.email}</TableCell>
          <TableCell>{empdata.obj.phone_number}</TableCell>
          <TableCell>{empdata.obj.department}</TableCell>
          <TableCell>{empdata.obj.project}</TableCell>
          <TableCell><button
                      type="submit"
                      className="btn btn-outline-primary btn-block rounded-pill"
                    >
                      Edit
                    </button></TableCell>
                    <TableCell><button
                      type="submit"
                      className="btn btn-outline-danger btn-block rounded-pill"
                    >
                      Delete
                    </button></TableCell>
          
    
        </TableRow>
         
      </TableBody>
    </Table>
    <Button
    color="primary"
    variant="contained"
    style={{ marginRight: 10 }}
    component={Link}
    to={`/`}
 >
    <AiFillHome />Back to Home
  </Button>
  </div>



// import { useState, useEffect } from "react";
// import {AiFillHome}  from "react-icons/ai";
// import { Link } from "react-router-dom";
// import "./get.css";


// const Employees_details = () => {

//   const data = localStorage.getItem("empdata")
//   const empdata = JSON.parse(data)
//   console.log(empdata)
//   return (
//     <div className="containerr table-responsive py-5">
//             <h3>  Welcome to.....</h3>
//       <div className="tablee">
//       <table className="table table-bordered table-hover">
//         <thead className="thead-darkk">
//           <tr>
//             <th scope="col">FirstName</th>
//             <th scope="col">LastName</th>
//             <th scope="col">Email</th>
//             <th scope="col">Mobile Number</th>
//             <th scope="col">Department</th>
//             <th scope="col">Project</th>
//             <th scope="col">Edit</th>
//             <th scope="col">Delete</th>
//           </tr>
//         </thead>
//         <tbody>
          
//             <tr >
//               <td>{empdata.obj.firstname}</td>
//               <td>{empdata.obj.lastname}</td>
//               <td>{empdata.obj.email}</td>
//               <td>{empdata.obj.phone_number}</td>
//               <td>{empdata.obj.department}</td>
//               <td>{empdata.obj.project}</td>
//               <td><Link to="/update">
//                 {" "}
//                 <button
//                   type='Edit'
//                   className="btn btn-outline-primary btn-block rounded-pill"
//                 >
//                   Edit
//                 </button></Link>
//               </td>
//               <td><Link to='/delete'>
//                 {" "}
//                 <button
//                   type="submit"
//                   className="btn btn-outline-danger btn-block rounded-pill"
//                 >
//                   Delete
//                 </button></Link>
//               </td>
//             </tr>
      
//         </tbody><br></br>
//         <Link to ='/' style={{textDecoration: 'none'}}><AiFillHome /> Back to Home</Link>
//       </table>
//     </div>
//     </div>
//   );
// };


  );
};

 export default  Employees_details;