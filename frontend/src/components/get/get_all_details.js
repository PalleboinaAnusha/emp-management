import React, { useEffect, useState } from "react";
import "./get.css";
import { AiFillHome } from "react-icons/ai";
import {
  Table,
  TableHead,
  TableCell,
  TableRow,
  TableBody,
  Button,
  makeStyles,
} from "@material-ui/core";
import { Link } from "react-router-dom";
const useStyles = makeStyles({
  table: {
    width: "100%",
    margin: "auto",
    backgroundColor: "skyblue",
  },
  thead: {
    "& > *": {
      fontSize: 15,
      background: "black",
      color: "#FFFFFF",
    },
  },
  
  row: {
    "& > *": {
      fontSize: 15,
    },
  },
});
function AllEmployeesList() {
  const [employees, setEmployeees] = useState([]);
  const classes = useStyles();
  useEffect(async () => {
    const emp = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };
    const response = await fetch(
      "http://localhost:4040/get_all_emp_details",
      emp
    );
    const data = await response.json();
    console.log(data);
    setEmployeees(data);
  }, []);

  return (
    <div>
      <div class="bg bg2"></div>
                <div class="bg bg3"></div>
      <Table className={classes.table}>
        <TableHead>
          <TableRow className={classes.thead}>
            <TableCell>First Name</TableCell>
            <TableCell>Last Name</TableCell>
            <TableCell>E-mail</TableCell>
            <TableCell>Mobile Number</TableCell>
            <TableCell>Department</TableCell>
            <TableCell>Project</TableCell>
            <TableCell>Edit Details</TableCell>
            <TableCell>Delete Employee</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {employees.map((employee) => (
            <TableRow className={classes.row} key={employee.id}>
              <TableCell>{employee.firstname}</TableCell>
              <TableCell>{employee.lastname}</TableCell>
              <TableCell>{employee.email}</TableCell>
              <TableCell>{employee.phone_number}</TableCell>
              <TableCell>{employee.department}</TableCell>
              <TableCell>{employee.project}</TableCell>
              <TableCell>
                {" "}
                <Link to={`./update/${employee._id}`}>
                  {" "}
                  <button
                    type="Edit"
                    className="btn btn-outline-primary btn-block rounded-pill"
                  >
                     Edit
                  </button>
                </Link>
              </TableCell>
              <TableCell>
                <Link to="/delete">
                  {" "}
                  <button
                    type="submit"
                    className="btn btn-outline-danger btn-block rounded-pill"
                  >
                    Delete
                  </button>
                </Link>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>

      <Button
        color="primary"
        variant="contained"
        style={{ marginRight: 10 }}
        component={Link}
        to={`/`}
     >
        <AiFillHome />Back to Home
      </Button>
    </div>
  );
}
// import { Component } from "react";

// //import EmployeeAllDetails from "../emp_details/details";
// import { AiFillHome } from "react-icons/ai";

// import "./get.css";
// import { Link } from "react-router-dom";
// import { HiOutlineSearch} from "react-icons/hi";
// //import SearchEmployees from "../search_employee/search";

// class AllEmployeesList extends Component {
//   state = { EmployeeData: [],
    
//    };


//   componentDidMount() {
//     this.ApiCall();
//   }

  
  
//   ApiCall = async () => {
//     const url = "http://localhost:4040/get_all_emp_details";
//     const option = {
//       method: "GET",
//       headers: {
//         "Content-Type": "application/json",
//         Accept: "application/json",
//       },
//     };
//     const response = await fetch(url, option);
//     console.log(response);
//     const data = await response.json();
//     console.log(data);
//     this.setState({ EmployeeData: data });
//   };
//   render() {
//     const { EmployeeData} = this.state;

//     return (
     

//       <div className="container11 table-responsive py-5">
//       <h3>  Welcome to.....</h3>
//       <div className="tb">
//       <table className="table table-bordered table-hover">
//         <thead className="thead-dark">

//           <tr>
//             <th scope="col">FirstName</th>
//             <th scope="col">LastName</th>
//             <th scope="col">Email</th>
//             <th scope="col">Mobile Number</th>
//             <th scope="col">Department</th>
//             <th scope="col">Project</th>
//             <th scope="col">Edit</th>
//             <th scope="col">Delete</th>
//           </tr>

//         </thead>
//         <tbody>
//           {EmployeeData.map((each) => (
//             <tr key={each.id}>
//               <td>{each.firstname}</td>
//               <td>{each.lastname}</td>
//               <td>{each.email}</td>
//               <td>{each.phone_number}</td>
//               <td>{each.department}</td>
//               <td>{each.project}</td>
//               <td>
//     <Link to={`./update/${each._id}`}>
//       {" "}
//       <button
//         type="Edit"
//         className="btn btn-outline-primary btn-block rounded-pill"
//       >
//         Edit
//       </button>
//     </Link>
//   </td>
//   <td>
//     <Link to="/delete">
//       {" "}
//       <button
//         type="submit"
//         className="btn btn-outline-danger btn-block rounded-pill"
//       >
//         Delete
//       </button>
//     </Link>
//   </td>
// </tr>
//           ))}
//         </tbody>
//         <br></br>
//         <Link to="/" style={{ textDecoration: "none" }}>
//           <AiFillHome /> Back to Home
//         </Link>
//       </table>
//     </div>
//     </div>
    //   <>
    //   <div className="Get-contianer">
    //   {/* <div className="form-search mb-5">
    //       <form className="example" >
    //         <input type="text" placeholder="Search Employees..." name="search" onChange={this.onChangeSearch} value={search}/>
    //         <button type="submit" onClick={this.onSubmitSearch}><HiOutlineSearch/></button>
    //       </form>
    //     </div> */}
    //     <h1 className="heading-line">Employee Details</h1>
        
    //     <div className="og-row og-li og-li-head">
    //       <div className="col og-li-col-2 ml-6 details-heading">
    //         FirstName
    //       </div>
    //       <div className="col og-li-col-3 text-center ml-6 details-heading">
    //        LastName
    //       </div>
    //       <div className="col og-li-col-4 text-center ml-6 details-heading">
    //        email
    //       </div>
    //       <div className="col og-li-col-5 text-center ml-6 details-heading">
    //        Mobile Number
    //       </div>
    //       <div className="col og-li-col-6 text-center ml-6 details-heading">
    //         Department
    //       </div>
    //       <div className="col og-li-col-6 text-center ml-6 details-heading">
    //        Project
    //       </div>
    //       <div className="col og-li-col-6 text-center ml-6 details-heading">
    //        Edit
    //       </div>
    //       <div className="col og-li-col-6 text-center ml-6 details-heading">
    //        Delete
    //       </div>
         
        
    //     </div>

    //     {EmployeeData.map((each) => (
    //       <EmployeeAllDetails each={each} key={each.id} />
          
         
    //     ))}
        
    //     {/* {searchFilter.map((eachEmployee) => (
    //         <SearchEmployees key={eachEmployee.id} each={eachEmployee} />
    //       ))} */}
        
    //   </div>
      
    //  <div class="all-buttons mt-5">
    //    {/* <Link to='./register'><button className="Add mr-5" > Add </button></Link> */}
    //    {/* <Link to={`./update/${each._id}`}><button className="Update mr-5">Update</button></Link> */}
    //    <Link to='./delete'><button className="delete mr-5">Delete</button></Link>

    //  </div>
     
    //   </>
//     );
//   }
// }

// import { useState, useEffect } from "react";
// import { AiFillHome } from "react-icons/ai";
// import { Link } from "react-router-dom";
// import "./get.css";

// const AllEmployeesList = () => {
//   const [employees, setEmployeees] = useState([]);

//   useEffect(async () => {
//     const emp = {
//       method: "GET",
//       headers: {
//         "Content-Type": "application/json",
//         Accept: "application/json",
//       },
//     };
//     const response = await fetch(
//       "http://localhost:4040/get_all_emp_details",
//       emp
//     );
//     const data = await response.json();
//     console.log(data);
//     setEmployeees(data);
//   }, []);
//   return (
    

//     <div className="container11 table-responsive py-5">
//       <h3>  Welcome to.....</h3>
//       <div className="tb">
//       <table className="table table-bordered table-hover">
//         <thead className="thead-dark">

//           <tr>
//             <th scope="col">FirstName</th>
//             <th scope="col">LastName</th>
//             <th scope="col">Email</th>
//             <th scope="col">Mobile Number</th>
//             <th scope="col">Department</th>
//             <th scope="col">Project</th>
//             <th scope="col">Edit</th>
//             <th scope="col">Delete</th>
//           </tr>

//         </thead>
//         <tbody>
//           {employees.map((employee) => (
//             <tr key={employee.id}>
//               <td>{employee.firstname}</td>
//               <td>{employee.lastname}</td>
//               <td>{employee.email}</td>
//               <td>{employee.phone_number}</td>
//               <td>{employee.department}</td>
//               <td>{employee.project}</td>
//               <td>
//     <Link to="/update">
//       {" "}
//       <button
//         type="Edit"
//         className="btn btn-outline-primary btn-block rounded-pill"
//       >
//         Edit
//       </button>
//     </Link>
//   </td>
//   <td>
//     <Link to="/delete">
//       {" "}
//       <button
//         type="submit"
//         className="btn btn-outline-danger btn-block rounded-pill"
//       >
//         Delete
//       </button>
//     </Link>
//   </td>
// </tr>
//           ))}
//         </tbody>
//         <br></br>
//         <Link to="/" style={{ textDecoration: "none" }}>
//           <AiFillHome /> Back to Home
//         </Link>
//       </table>
//     </div>
//     </div>
//   );
// };

export default AllEmployeesList;
