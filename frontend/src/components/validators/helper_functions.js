/**
 * This is a helper function to check and validate the email
 * @param {} email this is the parameter recieved to validate
 * @returns An object containing error messages.
 */
 function _email_checker(email) {
  const returnObject = {};
  // is it a string
  if (typeof email != "string") {
    returnObject.email = "email should be of string type";
    return returnObject;
  }
  if (email.length < 6) {
    returnObject.email = "email length should be at least 6 characters long";
    return returnObject;
  }
  if (email.length > 50) {
    returnObject.email = "email length should be a max of 50";
    return returnObject;
  }
  if (
    !email.match(
      /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    )
  ) {
    returnObject.email = "email should be of the correct format";
    return returnObject;
  }
  
  return returnObject;
  // const returnObject = {};
  // if (email === undefined) {
  //   returnObject.email = "email field is mandatory";
  //   return returnObject;
  // }
  // if (typeof email !== "string") {
  //   returnObject.email = "email must be a string";
  //   return returnObject;
  // }
  // if (!email.toLowerCase().match(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
  //   returnObject.email = "email is supposed to be in valid format";
  //   return returnObject;
  // }
  // if (!email.endsWith("@gmail.com")) {
  //   returnObject.email = "Enter a valid mail that ends with @gmail.com";
  //   return returnObject;
  // }
  // return returnObject;
}

/**
 * This is a helper function to check and validate the mobile_no
 * @param {} mobile_no this is the parameter recieved to validate
 * @returns An object containing error messages.
 */
function _mobile_no_checker(phone_number) {
  const returnObject = {};
  if (phone_number === undefined) {
    returnObject.phone_number = "Mobile Number field is mandatory";
    return returnObject;
  }
  if (typeof phone_number !== "number") {
    returnObject.phone_number = "Mobile Number should be a Number type";
    return returnObject;
  }
  if (!Number.isInteger(phone_number)) {
    returnObject.phone_number = "Mobile Number have no special characters points " + phone_number;
    return returnObject;
  }
  if (phone_number.toString().length !== 10) {
    returnObject.phone_number = "Mobile number should have 10 digits: " + phone_number;
    return returnObject;
  }
  if (
    !phone_number.match(
      /^(\+91[\-\s]?)?[0]?(91)?[789]\d{9}$/)
  ) {
    returnObject.email = "email should be of the correct format";
    return returnObject;
  }
 
  return returnObject;
}

/**
 * This is a helper function to check and validate the firstname
 * @param {} firstname this is the parameter recieved to validate
 * @returns An object containing error messages.
 */
function _firstname_checker(firstname) {
  const returnObject = {};

  if (!firstname) {
    returnObject.firstname = "First Name is required";
    console.log("firstname" + typeof firstname);
    return returnObject;
  }
  if (typeof firstname != "string") {
    returnObject.firstname = "firstname should be of string type";
    return returnObject;
  }

  if (firstname.length < 3) {
    returnObject.firstname =
      "firstname length should be at least 4 characters long";
    return returnObject;
  }

  if (firstname.length > 25) {
    returnObject.firstname = "firstname length shouldn't be 25 characters long";
    return returnObject;
  }

  if (!firstname.match(/^[a-zA-Z ]+$/)) {
    returnObject.firstname = "firstname should be of the correct format";
    return returnObject;
  }

  return returnObject;
}

/**
 * This is a helper function to check and validate the lastname
 * @param {} lastname this is the parameter recieved to validate
 * @returns An object containing error messages.
 */
 function _lastname_checker(lastname) {
  const returnObject = {};
  
    if (typeof lastname != "string") {
      returnObject.lastname = "lastname should be of string type";
      return returnObject;
    }
  
    if (lastname.length < 3) {
      returnObject.lastname =
        "lastname length should be at least 4 characters long";
      return returnObject;
    }
  
    if (lastname.length > 25) {
      returnObject.lastname = "lastname length shouldn't be 25 characters long";
      return returnObject;
    }
  
    if (!lastname.match(/^[a-zA-Z ]+$/)) {
      returnObject.lastname = "lastname should be of the correct format";
      return returnObject;
    }
  
    return returnObject;
}


/**
 * This is a helper function to check and validate the department
 * @param {} department this is the parameter recieved to validate
 * @returns An object containing error messages.
 */
 function _department_checker(department) {
  const returnObject = {};
  
  if (typeof department != "string") {
    returnObject.department = "department should be of string type";
    return returnObject;
  }

  if (department.length < 3) {
    returnObject.department =
      "department length should be at least 4 characters long";
    return returnObject;
  }

  if (department.length > 25) {
    returnObject.department = "department length shouldn't be 25 characters long";
    return returnObject;
  }

  if (!department.match(/^[a-zA-Z ]+$/)) {
    returnObject.department = "department should be of the correct format";
    return returnObject;
  }

  return returnObject;
}

//   const returnObject = {};
//   // is it a string
//   if (department === undefined) {
//     returnObject.department = "Please give Your department";
//     return returnObject;
//   }
//   if (typeof department !== "string") {
//     returnObject.department = "department should be of string type";
//     return returnObject;
//   }
//   if (department.length === 0) {
//     returnObject.department = "department field is mandatory";
//     return returnObject;
//   }
//   return returnObject;
// }

/**
 * This is a helper function to check and validate the lastname
 * @param {} lastname this is the parameter recieved to validate
 * @returns An object containing error messages.
 */
 function _project_checker(project) {
  const returnObject = {};
  
  if (typeof project != "string") {
    returnObject.project = "project should be of string type";
    return returnObject;
  }

  if (project.length < 3) {
    returnObject.project =
      "project length should be at least 4 characters long";
    return returnObject;
  }

  if (project.length > 25) {
    returnObject.project = "project length shouldn't be 25 characters long";
    return returnObject;
  }

  if (!project.match(/^[a-zA-Z ]+$/)) {
    returnObject.project = "project should be of the correct format";
    return returnObject;
  }

  return returnObject;
}

  // const returnObject = {};
  // // is it a string
  // if (project === undefined) {
  //   returnObject.project = "Please give Your Product Name";
  //   return returnObject;
  // }
  // if (typeof project !== "string") {
  //   returnObject.project = "Product Name should be of string type";
  //   return returnObject;
  // }
  // if (project.length === 0) {
  //   returnObject.project = "Product Name field is mandatory to fill";
  //   return returnObject;
  // }
  // return returnObject;
//}

/**
 * This is a helper function to count the keys in object recieved
 */
function _return_object_keys(obj) {
  
  let returnArray = [];
  for (let key in obj) {
    returnArray.push(key);
  }

  return returnArray;
}


export {

  _firstname_checker,
  _email_checker,
  _mobile_no_checker,
  _return_object_keys,
  _lastname_checker,
  _department_checker,
  _project_checker

};
