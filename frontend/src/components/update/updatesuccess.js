import './updatesuccess.css';
import {AiFillHome}  from "react-icons/ai";
import { Link } from "react-router-dom";

function UpdatedSuccess(){
  const data = localStorage.getItem("updatedata")
  const empdata = JSON.parse(data)
    return (
        <div className="animation-ctn">
        <div className="icon icon--order-success svg">
          <svg xmlns="http://www.w3.org/2000/svg" width="154px" height="154px">
            <g fill="none" stroke="#22AE73" strokeWidth={2}>
              <circle cx={77} cy={77} r={72} style={{strokeDasharray: '480px, 480px', strokeDashoffset: '960px'}} />
              <circle id="colored" fill="#22AE73" cx={77} cy={77} r={72} style={{strokeDasharray: '480px, 480px', strokeDashoffset: '960px'}} />
              <polyline className="st0" stroke="#fff" strokeWidth={10} points="43.5,77.8 63.7,97.9 112.2,49.4 " style={{strokeDasharray: '100px, 100px', strokeDashoffset: '200px'}} />
            </g>
          </svg>
        </div>
        <br />
        <h2>Employee Details Updated Successfully</h2>
        
        <Link to ='/' style={{textDecoration: 'none'}}><AiFillHome />Back to Home</Link> 
      </div>
    )
}
export {UpdatedSuccess}