
import {Navbar, Nav} from 'react-bootstrap';
import { ImHome } from "react-icons/im";


function Navigation(){
    return (

  
          <Navbar bg="success" variant="dark" expand="sm" fixed="right">
                                <Navbar.Brand href="/"><ImHome /> Employee Management</Navbar.Brand>
                                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                                <Navbar.Collapse id="responsive-navbar-nav">
                                    <Nav className="mr-auto">
                                    
                                    <Nav.Link href="/all_emp_details">All Employees</Nav.Link>
                                    <Nav.Link href="/register">Register Employee</Nav.Link>
                                    <Nav.Link href="/getempdata">Login Employee Details</Nav.Link>
                                    
                                    </Nav>
                                </Navbar.Collapse>
                            </Navbar>
                           
      );
}

export {Navigation}