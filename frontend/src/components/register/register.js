
import React, { Component } from "react";
import {
  _department_checker,
  _email_checker,
  _firstname_checker,
  _lastname_checker,
  _mobile_no_checker,
  _project_checker,
  _return_object_keys,
} from "../validators/helper_functions.js";
import { Link } from "react-router-dom";
import { AiFillHome } from "react-icons/ai";
import "./register.css";

class Registration extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstname: " ",
      lastname: " ",
      phone_number: " ",
      email: " ",
      department: " ",
      project: " ",
      // error messages,
      phone_number_err: " ",
      password_err: "",
      department_err: "",
      firstname_err: "",
      lastname_err: "",
      email_err: "",
      project_err: "",
    };
    // this.onImageChange = this.onImageChange.bind(this);
  }

  signUpSuccess = () => {
    const { history } = this.props;
    history.push("/registeredSuccessfully");
  };
  apiCallFail = (data) => {
    this.setState({ error_msg: data.err_msg });
  };

  registerApiCall = async (event) => {
    event.preventDefault();
    console.log(this.state);

    const {
      firstname,
      lastname,
      phone_number,
      email,
      department,
      project,
      image,
    } = this.state;
    const url = "http://localhost:4040/create_profile";
    const userDetails = {
      firstname,
      lastname,
      phone_number: parseInt(phone_number),
      email,
      department,
      project,
      image,
    };
    const emp = {
      method: "POST",
      body: JSON.stringify(userDetails),
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };
    const response = await fetch(url, emp);
    const data = await response.json();
    console.log(data);
    if (data.status === 200) {
      localStorage.setItem("data", JSON.stringify(data));
      this.signUpSuccess();
    } else {
      this.apiCallFail(data);
    }
  };

  changefirstname = (event) => {
    this.setState({ firstname: event.target.value, firstname_err: "" });
  };
  // handle onBlur to password in JSX
  validatefirstname = () => {
    const firstname = this.state.firstname;
    const firstname_errors = _firstname_checker(firstname);
    const is_firstname_validated =
      _return_object_keys(firstname_errors).length === 0;
    console.log(firstname_errors);
    console.log(is_firstname_validated);
    if (!is_firstname_validated) {
      // firstname validation failed
      this.setState({ firstname_err: firstname_errors.firstname });
    }
  };

  changelastname = (event) => {
    this.setState({ lastname: event.target.value, lastname_err: "" });
  };

  // handle onBlur to password in JSX
  validatelastname = () => {
    const lastname = this.state.lastname;
    const lastname_errors = _lastname_checker(lastname);
    const is_lastname_validated =
      _return_object_keys(lastname_errors).length === 0;
    console.log(lastname_errors);
    console.log(is_lastname_validated);
    if (!is_lastname_validated) {
      // firstname validation failed
      this.setState({ lastname_err: lastname_errors.lastname });
    }
  };

  changephone_number = (event) => {
    let newphone_number = event.target.value;
    newphone_number = newphone_number.replace(/\D/g, "");
    this.setState({ phone_number: newphone_number, phone_number_err: " " });
  };

  // handle on blur to emp_id input in JSX
  validatephone_number = () => {
    const phone_number = this.state.phone_number;
    const phone_number_errors = _mobile_no_checker(parseInt(phone_number));
    const is_phone_number_validated =
      _return_object_keys(phone_number_errors).length === 0;
    console.log(phone_number_errors);
    console.log(is_phone_number_validated);
    if (!is_phone_number_validated) {
      // emp_id validation failed
      this.setState({ phone_number_err: phone_number_errors.phone_number });
    }
  };

  changeEmail = (event) => {
    this.setState({ email: event.target.value, email_err: "" });
  };

  // handle on blur to emp_id input in JSX
  validateemail = () => {
    const email = this.state.email;
    const email_errors = _email_checker(email);
    const is_email_validated = _return_object_keys(email_errors).length === 0;
    console.log(email_errors);
    console.log(is_email_validated);
    if (!is_email_validated) {
      // emp_id validation failed
      this.setState({ email_err: email_errors.email });
    }
  };

  changedepartment = (event) => {
    this.setState({ department: event.target.value, department_err: "" });
  };
  // handle onBlur to password in JSX
  validatedepartment = () => {
    const department = this.state.department;
    const department_errors = _department_checker(department);
    const is_department_validated =
      _return_object_keys(department_errors).length === 0;
    console.log(department_errors);
    console.log(is_department_validated);
    if (!is_department_validated) {
      // department validation failed
      this.setState({ department_err: department_errors.department });
    }
  };

  changeproject = (event) => {
    this.setState({ project: event.target.value, project_err: "" });
  };

  // handle onBlur to password in JSX
  validateproject = () => {
    const project = this.state.project;
    const project_errors = _project_checker(project);
    const is_project_validated =
      _return_object_keys(project_errors).length === 0;
    console.log(project_errors);
    console.log(is_project_validated);
    if (!is_project_validated) {
      // project validation failed
      this.setState({ project_err: project_errors.project });
    }
  };

  

  render() {
    return (
      <div className="response">
        <div class="bg bg2"></div>
        <div class="bg bg3"></div>

        <div className="form">
          <form className="register-form"></form>

          <h6 className="register2">
            <img src="https://www.seekpng.com/png/detail/334-3349723_9-business-client-icon-images-moodle-user.png" />
            Register Employee Details
          </h6>
          <form className="login-form" onSubmit={this.registerApiCall}>
            <input
              type="text"
              placeholder="First Name"
              required
              onChange={this.changefirstname}
              onBlur={this.validatefirstname}
            />
            <p style={{ color: "red" }}>{this.state.firstname_err}</p>
            <input
              type="text"
              placeholder="Last Name"
              required
              onChange={this.changelastname}
              onBlur={this.validatelastname}
            />
            <p style={{ color: "red" }}>{this.state.lastname_err}</p>
            <input
              type="email"
              placeholder="E-mail"
              required
              onChange={this.changeEmail}
              onBlur={this.validateemail}
            />
            <p style={{ color: "red" }}>{this.state.email_err}</p>
            <input
              type="number"
              placeholder="Mobile Number"
              required
              onChange={this.changephone_number}
              onBlur={this.validatephone_number}
            />
            <p style={{ color: "red" }}>{this.state.phone_number_err}</p>
            <input
              type="text"
              placeholder="Department"
              required
              onChange={this.changedepartment}
              onBlur={this.validatedepartment}
            />
            <p style={{ color: "red" }}>{this.state.department_err}</p>
            <input
              type="text"
              placeholder="Project"
              required
              onChange={this.changeproject}
              onBlur={this.validateproject}
            />
            <p style={{ color: "red" }}>{this.state.project_err}</p>
            <br></br>
            <button
              type="submit"
              className="btn btn-outline-success btn-block rounded-pill"
            >
              Register
            </button>
          </form>
          <br></br>
          <Link to="/" style={{ textDecoration: "none" }}>
            <AiFillHome /> Back to Home
          </Link>
        </div>
      </div>
    );
  }
}
export default Registration;
