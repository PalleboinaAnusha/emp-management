import React from "react";
import {
  _email_checker,
  _return_object_keys,
} from "../validators/helper_functions";
import { Link } from "react-router-dom";
import "./delete.css";
import { AiFillHome } from "react-icons/ai";

class Delete extends React.Component {
  state = {
    email: " ",
    email_err: "",
    email_db_err: "",
  };
  Success = () => {
    const { history } = this.props;
    history.push("/deletedsuccessfully");
  };
  apiCallFail = (data) => {
    this.setState({ email_db_err: data.msg });
  };
  changeEmail = (event) => {
    this.setState({ email: event.target.value, email_err: "" });
  };
  // handle on blur to emp_id input in JSX
  validateemail = () => {
    const email = this.state.email;
    const email_errors = _email_checker(email);
    const is_email_validated = _return_object_keys(email_errors).length === 0;
    console.log(email_errors);
    console.log(is_email_validated);
    if (!is_email_validated) {
      // emp_id validation failed
      this.setState({ email_err: email_errors.email });
    }
  };
  deleteApiCall = async (event) => {
    event.preventDefault();
    console.log(this.state);

    const { email } = this.state;
    const url = "http://localhost:4040/delete_details";
    const userDetails = {
      email,
    };
    const option = {
      method: "DELETE",
      body: JSON.stringify(userDetails),
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };
    const response = await fetch(url, option);
    const data = await response.json();
    console.log(data);
    if (data.status === 200) {
      localStorage.setItem("deletedata", JSON.stringify(data));
      this.Success();
    } else {
      this.apiCallFail(data);
    }
  };
  render() {
    return (
      <div className="response">
        <div class="bg bg2"></div>
        <div class="bg bg3"></div>
        <div className="login-page">
          <div className="form">
            <form className="register-form"></form>
            <h5 className="register2">
              <img src="https://www.seekpng.com/png/detail/334-3349723_9-business-client-icon-images-moodle-user.png" />
              Delete
            </h5>
            <form className="login-form" onSubmit={this.deleteApiCall}>
              <input
                type="email"
                placeholder="E-mail"
                required
                onChange={this.changeEmail}
                onBlur={this.validateemail}
              />
              <p style={{ color: "red" }}>{this.state.email_db_err}</p>
              <p style={{ color: "red" }}>{this.state.email_err}</p>
              <button
                type="submit"
                className="btn btn-outline-danger btn-block rounded-pill"
              >
                Delete
              </button>
            </form>
            <br></br>
            <Link to="/" style={{ textDecoration: "none" }}>
              <AiFillHome /> Back to Home
            </Link>
          </div>
        </div>
      </div>
    );
  }
}

export default Delete;
