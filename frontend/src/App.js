import { BrowserRouter } from "react-router-dom";
import { Switch } from "react-router-dom";
import { Route } from "react-router-dom";
import Registration from "./components/register/register.js";
import Updatedata from "./components/update/update.js";
import Mainpage from "./components/mainpage/mainpage.js";
import Delete from "./components/delete/delete.js";
import { DeleteSuccess } from "./components/delete/deletesuccess.js";
import { RegisterSuccess } from "./components/register/registersuccess.js";
import { UpdatedSuccess } from "./components/update/updatesuccess.js";
import Getempdata from "./components/get/emp_details.js";
import Profilepage from "./components/get/profilepage.js";
import { Navigation } from "./components/navigation/navigation.js";


import AllEmployeesList from "./components/get/get_all_details.js";
function App() {
  return (
    <>
    <Navigation/>
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={Mainpage} />
        <Route path="/register" exact component={Registration} />
        <Route path="/registeredSuccessfully" exact component={RegisterSuccess}/>
        <Route path="/update/:id" exact component={Updatedata} />
        <Route path="/delete" exact component={Delete} />
        <Route path="/deletedsuccessfully" exact component={DeleteSuccess} />
        <Route path="/updatedsuccessfully" exact component={UpdatedSuccess} />
        <Route path="/getempdata" exact component={Getempdata} />
        <Route path="/profiledetails" exact component={Profilepage} />
        <Route path='/all_emp_details' exact component={AllEmployeesList}/>
      </Switch>
    </BrowserRouter>
    </>
  );
}

export default App;
