import mongoose from "mongoose";

//creating Schema
const UserSchema = mongoose.Schema({
    firstname :        {type: String, required : true},
    lastname :         {type: String, required : true},
    phone_number :     {type : Number, required: true, unique : true},
    department :       {type: String, required : true},
    email :            {type: String, required : true,unique : true},
    project :          {type: String, required : true}
})

//creating model
const EMP_MODEL = mongoose.model("EMP_MODEL", UserSchema)


export {EMP_MODEL}
