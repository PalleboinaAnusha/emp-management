//importing external dependencies
import express      from "express";
import bodyParser	from "body-parser";
import mongoose 	from "mongoose";
import cors from 'cors';
//importing internal dependencies
// handlers
import { register_handler }       from "./path_handlers/index.js";
import { get_details }            from "./path_handlers/index.js";
import { update_details_handler } from "./path_handlers/index.js";
import { delete_handler }         from "./path_handlers/index.js";
import { employee_data_validator } from "./utils/emp_validators.js";
import { get_employee_details_handler } from "./path_handlers/get/get_details.js";

const app = express()
app.use(cors({origin: "*"}))
app.use(bodyParser.json());                          //Returns middleware that only parses json
app.use(bodyParser.urlencoded({ extended: true }));  //Returns middleware that only parses urlencoded bodies
app.use(express.json());

/************POST CALL ***********/
app.post("/create_profile", employee_data_validator,register_handler)

app.post("/get_emp_details", get_details)
/************GET CALL************/
app.get("/get_all_emp_details", get_employee_details_handler)

/************PUT CALL************/
app.put('/update/:id', update_details_handler)

/***********DELETE CALL**********/
app.delete('/delete_details', delete_handler)

//listening to server

mongoose.connect("mongodb+srv://ANUSHA:ANUSHA@cluster0.s54yo.mongodb.net/ANUSHA?retryWrites=true&w=majority")
.then(()=>{
    app.listen(4040, ()=>{
        console.log("server started and listening in port 4040")
    })
})
.catch((err)=>{
    console.log("unable to connect db" , err)
})
