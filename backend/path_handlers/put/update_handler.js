import { response } from "express";
import { EMP_MODEL } from "../../db/schema.js";
/**
 * This function is used to update  the user email address.
 * If the user exists then checks for user email address
 * @param {*} request username,new_email from body
 * @param {*} response sends statuscode and status_message
 */
function update_details_handler(request, response) {
  

  // EMP_MODEL.findOne({ email}, (err, dataObj) => {
  //   if (err) {
  //     response.status(500).send("db error", err);
  //   } else {
  //     if (dataObj === null) {
  //       response.status(400).send("Unable to find data");
  //     } else {
  //       dataObj.updateOne({ $set: {firstname:new_first_name,phonenumber:new_phone_number,lastname:new_last_name } }, (err,updateobj) => {
  //         if (err) {
  //           response.send("unable to update email");
  //         } else {
  //           console.log(updateobj.firstname)
  //           console.log(new_last_name)
  //           console.log(new_phone_number)
  //           response.status(200).send({msg :"mobile_number updated successfully success", status:200,old_firstname:dataObj.firstname, old_number:dataObj.phone_number, old_lastname:dataObj.lastname,new_phone_number:new_phone_number,new_first_name:new_first_name,new_last_name:new_last_name});
  //         }
  //       });
  //     }
  //   }
  // });
   const id=request.params.id
   console.log(id);
  EMP_MODEL.findByIdAndUpdate(id,{$set:request.body},(err,updateobj)=>{
    if(err){
      response.send(err)
    }
    else{
      response.send({msg:updateobj})
    }
  })
}

export { update_details_handler };
