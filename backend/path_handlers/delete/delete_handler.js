import { EMP_MODEL } from "../../db/schema.js";
/**
 * This is a  function to soft delete the user_details
 * @param request express request object
 * @param response express response object
 * @returns An object containing result or error messages.
 */
function delete_handler(request, response) {
  const { email } = request.body;
  EMP_MODEL.findOne({ email }, (err, dataObj) => {
    if (err) {
      response.status(500).send("Database error");
    } else {
      if (dataObj == null) {
        response.status(401).send({ msg: "Email does not exist" });
      } else {
        dataObj.delete((err) => {
          if (err) {
            response.send({err_msg:"unable to delete data",status:401});
          } else {
            response.send({msg:"Deleted successfully",status:200,  name : dataObj.firstname});
          }
        });
      }
    }
  });
}
export { delete_handler };
